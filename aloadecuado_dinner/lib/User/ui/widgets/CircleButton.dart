import 'package:flutter/material.dart';

class CircleButton extends StatefulWidget{

  final VoidCallback onPressed;
  bool mini;
  var icon;
  double iconSize;
  var color;

  CircleButton(this.mini, this.icon, this.iconSize, this.color, @required this.onPressed);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CircleButton();
  }
}

class _CircleButton extends State<CircleButton>{


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Expanded(
        child: FloatingActionButton(
            onPressed: widget.onPressed,
            mini: widget.mini,
            backgroundColor: widget.color,
          child: Icon(widget.icon,
          size: widget.iconSize,
          color: Color(0xFF4268D3),),
          heroTag: null,
        ));
  }



}