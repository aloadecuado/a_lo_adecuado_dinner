import 'dart:typed_data';

import 'package:aloadecuado_dinner/User/bloc/UserBloc.dart';
import 'package:aloadecuado_dinner/User/model/User.dart';
import 'package:aloadecuado_dinner/User/ui/widgets/ButtonGreen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:generic_bloc_provider/generic_bloc_provider.dart';

import '../../../MenuTabBar.dart';
import '../../../SelectedFlow.dart';

class SignUp extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SignUp();
  }
}

class _SignUp extends State<SignUp>{

  UserBloc userBloc;
  @override
  Widget build(BuildContext context) {

    userBloc = BlocProvider.of(context);


    userBloc.signOut();
    return handleCurrentSession(context);
  }

  Widget handleCurrentSession(BuildContext context){
    return StreamBuilder(
      stream: userBloc.authStatus,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        //snapshot- data - Object User
        if(!snapshot.hasData || snapshot.hasError) {
          return screenSignUp(context);
        } else {
          return SelectedFlow();
        }
      },
    );

  }

  Widget screenSignUp(BuildContext context){

    return Scaffold(
        body: Stack(children: <Widget>[
          //Image.asset("assets/img/repeta.jpg"),
          Column(
            children: <Widget>[

              ButtonGreen(text: "Vamos a por ello", onPressed: () {
                //userBloc.signOut();
                userBloc.signIn().then((FirebaseUser uuser){
                  userBloc.createUserData(User(uuser.uid, uuser.displayName, uuser.email, uuser.photoUrl, 57, "", DateTime.now(), null));

                });

              },
                height: 50.0,)

            ],
            mainAxisAlignment: MainAxisAlignment.center,)

        ],
        )

    );


  }
}

