import 'package:firebase_auth/firebase_auth.dart';

import 'AuthFirebaseApi.dart';

class AuthFirebaseRepository{

  final _firebaseAuthAPI = AuthFirebaseApi();

  Future<FirebaseUser> signInFirebase() => _firebaseAuthAPI.signIn();

  signOut() => _firebaseAuthAPI.signOut();
}