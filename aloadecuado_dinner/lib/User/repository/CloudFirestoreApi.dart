import 'package:aloadecuado_dinner/User/model/User.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class CloudFirestoreApi {

  final String USERS = "users";
  final String PLACES = "places";

  final Firestore _db = Firestore.instance;
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<User> createUserData(User user) async{

    DocumentReference ref = _db.collection(USERS).document(user.uid);
    await ref.get().then((DocumentSnapshot document)  {

      if(document.data == null){

        setUser(USERS, user.uid, user);
      }else{
        User user = User("","","","",0,"",DateTime.now(),null);

        user.setUser(document);
      }

    }).catchError((bool, error) {


    });

    return user;

  }



  Future<User> setUser(String collection ,String document , User user)async{
    DocumentReference ref = _db.collection(collection).document(document);
    await ref.setData(user.getMapNoLocations(), merge: true).catchError((bool, error) {

    });

    return user;

  }
}