
import 'package:aloadecuado_dinner/User/model/User.dart';
import 'package:aloadecuado_dinner/User/repository/CloudFirestoreApi.dart';

class CloudFirestoreRepository {

  final _cloudFirestoreAPI = CloudFirestoreApi();

  void createUserData(User user) => _cloudFirestoreAPI.createUserData(user);



}