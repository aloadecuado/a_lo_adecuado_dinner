import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';


class Location {
   String id;
   String name;
   String adrress;
   double lat;
   double log;

  Location(this.id, this.name, this.adrress, this.lat, this.log);


  void setLocation(DocumentSnapshot documentSnapshot){
    id = documentSnapshot.data["id"];
    name = documentSnapshot.data["name"];
    adrress = documentSnapshot.data["adrress"];
    lat = documentSnapshot.data["lat"];
    log = documentSnapshot.data["log"];
  }
}