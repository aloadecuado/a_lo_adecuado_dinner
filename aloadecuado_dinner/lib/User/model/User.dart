import 'package:aloadecuado_dinner/User/model/Location.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';


class User {
   String uid;
   String name;
   String email;
   String photoURL;
   int indicativoCel;
   String numberCel;
   DateTime createDate;
   List<Location> locations;

  User(this.uid, this.name, this.email, this.photoURL, this.indicativoCel,
      this.numberCel, this.createDate, this.locations);

  Map<String, dynamic> getMapNoLocations(){

    return {"uid":uid,"name":name,"email":email,"photoURL":photoURL,"indicativoCel":indicativoCel,"numberCel":numberCel,"createDate":createDate};
  }

  void setUser(DocumentSnapshot documentSnapshot){

    this.uid = documentSnapshot.data["uid"];
    this.name = documentSnapshot.data["name"];
    this.email = documentSnapshot.data["email"];
    this.photoURL = documentSnapshot.data["photoURL"];
    this.indicativoCel = documentSnapshot.data["indicativoCel"];
    this.numberCel = documentSnapshot.data["numberCel"];
    this.createDate = documentSnapshot.data["createDate"];
    this.uid = documentSnapshot.data["documentSnapshot"];

    Map map = documentSnapshot.data["locations"];

    /*map.forEach((p){

    });*/



  }



}