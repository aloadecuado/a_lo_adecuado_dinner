import 'dart:async';

import 'package:aloadecuado_dinner/User/model/User.dart';
import 'package:aloadecuado_dinner/User/repository/AuthFirebaseRepository.dart';
import 'package:aloadecuado_dinner/User/repository/CloudFirestoreRepository.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:generic_bloc_provider/generic_bloc_provider.dart';

class UserBloc implements Bloc {

  final authRepository = AuthFirebaseRepository();
  final _cloudFirestoreRepository = CloudFirestoreRepository();

  //1. SignIn a la aplicación Google
  Future<FirebaseUser> signIn() =>authRepository.signInFirebase();
  //2. Verificacion estado de la autenticacion
  Stream<FirebaseUser> streamFirebase = FirebaseAuth.instance.onAuthStateChanged;
  Stream<FirebaseUser> get authStatus => streamFirebase;
  //3. Crear usuario
  void createUserData(User user) => _cloudFirestoreRepository.createUserData(user);


  //4. get User Info

  StreamController<User> placeSelectedStreamController =  StreamController<User>();
  Stream<User> get placeSelectedStream => placeSelectedStreamController.stream;
  signOut() {authRepository.signOut();}
  @override
  void dispose() {
    // TODO: implement dispose
  }
}