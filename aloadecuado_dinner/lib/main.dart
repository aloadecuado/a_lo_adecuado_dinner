import 'package:aloadecuado_dinner/User/ui/screens/SignUp.dart';
import 'package:flutter/material.dart';
import 'package:generic_bloc_provider/generic_bloc_provider.dart';

import 'User/bloc/UserBloc.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        child: MaterialApp(
          title: 'Flutter Demo',
          //home: PlatziTripsCupertino(),
          home: SignUp(),
        ),
        bloc: UserBloc());;
  }


}

