import 'package:flutter/material.dart';
import 'User/ui/widgets/ButtonGreen.dart';
import 'Util/SetAndGetDataStorage.dart';
class SelectedFlow extends StatefulWidget{

  String rolChefString = "Chef";
  String rolComensalString = "Comensal";
  SetAndGetDataStorage setAndGetDataStorage;
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    setAndGetDataStorage = SetAndGetDataStorage();

    setAndGetDataStorage.getRol().then((user){

    });
    return _SelectedFlow();
  }

}

class _SelectedFlow extends State<SelectedFlow>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return selectedFlow();
  }

Widget selectedFlow(){

    return Scaffold(
      body: Stack(
        children: <Widget>[
        Column(children: <Widget>[
          ButtonGreen(text: "Chef", onPressed: () {
            widget.setAndGetDataStorage.setRol(widget.rolChefString);

          },
            height: 50.0,),
          ButtonGreen(text: "Comensal", onPressed: () {
            widget.setAndGetDataStorage.setRol(widget.rolComensalString);
          },
            height: 50.0,)
        ],
        mainAxisAlignment: MainAxisAlignment.center,)
      ],),
    );
}

}