import 'package:shared_preferences/shared_preferences.dart';

class SetAndGetDataStorage {

  String keyStorage = "ALoAdecuadoKey";

  Future setRol(String rol) async {
    final prefs = await SharedPreferences.getInstance();

// set value
    prefs.setString(keyStorage, rol);
  }

  Future<String> getRol() async{

    final prefs = await SharedPreferences.getInstance();

// set value
    return prefs.getString(keyStorage);
  }
}